package com.andrew;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import com.andrew.Item;

public class TestItem {
	
	@Test
	public void testThatItemInstantiationCorrectlyCalculatesVatForTheItem(){
		Item item = new Item("1", "Bag of ice", "100");
		
		BigDecimal expectedUnitPrice = new BigDecimal("100");
		BigDecimal expectedVat = new BigDecimal("14.00");
		BigDecimal expectedUnitPricePlusVat = new BigDecimal("114.00");
		
		assertEquals("1", item.getProductId());			
		assertEquals("Bag of ice", item.getDescription());			
		assertEquals(expectedUnitPrice, item.getUnitPrice());			
		assertEquals(expectedVat, item.getVat());			
		assertEquals(expectedUnitPricePlusVat, item.getUnitPricePlusVat());	
		assertEquals(1, item.getCount());
		item.incrementCount();
		assertEquals(2, item.getCount());
		
	}

	@Test
	public void testThatItemInstantiationCorrectlyCalculatesVatForTheItem2(){
		Item item = new Item("1", "Bag of ice", "25");
		
		BigDecimal expectedUnitPrice = new BigDecimal("25");
		BigDecimal expectedVat = new BigDecimal("3.50");
		BigDecimal expectedUnitPricePlusVat = new BigDecimal("28.50");
		expectedVat.setScale(2, BigDecimal.ROUND_HALF_UP);
		
		assertEquals("1", item.getProductId());			
		assertEquals("Bag of ice", item.getDescription());	
		assertEquals(expectedUnitPrice, item.getUnitPrice());	
		assertEquals(expectedVat, item.getVat());			
		assertEquals(expectedUnitPricePlusVat, item.getUnitPricePlusVat());		
		assertEquals(1, item.getCount());
		item.incrementCount();
		assertEquals(2, item.getCount());
	}
	
}
