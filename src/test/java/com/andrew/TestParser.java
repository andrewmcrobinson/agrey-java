package com.andrew;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;

import org.junit.Test;

import com.andrew.Parser;

public class TestParser {

	String expectedOutputFilepath = "src/main/resources/"+"expected.txt";
	String inputFilepath = "src/main/resources/"+"invoiceItems.txt";
	
	@Test
	public void test() throws IOException{
		
		List<String> expectedLines = Files.readAllLines(FileSystems.getDefault().getPath(".", expectedOutputFilepath), Charset.defaultCharset());
		
		Parser parser = new Parser();		
		List<String> actualLines = parser.buildOutputFromCsv(inputFilepath);

		if (actualLines.size() != expectedLines.size()){
			fail("actualLines.size():"+actualLines.size()+" != expectedLines.size():"+expectedLines.size());
		}
		
		int i=0;
		for (String actualLine: actualLines){			
			
			String expectedLine = expectedLines.get(i);
			
			if (!actualLine.equals(expectedLine)){
				System.out.println("line "+i+": actualLine not equal to expectedLine");
				System.out.println("line "+i+": actualLine:'"+actualLine+"'");
				System.out.println("line "+i+": expectedLine:'"+expectedLine+"'");
				System.out.println();
			}

			i++;
			
		}

	}
}
