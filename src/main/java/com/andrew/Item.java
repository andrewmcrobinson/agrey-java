package com.andrew;

import java.math.BigDecimal;

/**
 * This is a helper class used by Parser.
 * It stores the data for a specific productId and
 * also calculates the Vat amount based on the unitPrice.
 * 
 * @author andrew
 *
 */
public class Item {
	
	private String productId;
	private String description;
	private BigDecimal unitPrice;
	private BigDecimal vat;
	private int count;
	
	private BigDecimal vatRate = new BigDecimal("0.14");
	
	public Item(String productId, String description, String unitPrice) {
		super();
		
		this.productId = productId;
		this.description = description;

		this.unitPrice = new BigDecimal(unitPrice);
		this.unitPrice.setScale(2, BigDecimal.ROUND_HALF_UP);			
		
		this.vat = this.unitPrice.multiply(vatRate);
		count=1;
	
	}
	
	public void incrementCount() {
		count++;
	}
	
	public int getCount() {
		return count;
	}
	
	public String getProductId() {
		return productId;
	}

	public String getDescription() {
		return description;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public BigDecimal getVat() {
		return vat;
	}

	public BigDecimal getUnitPricePlusVat() {
		return unitPrice.add(vat);
	}
	
}
