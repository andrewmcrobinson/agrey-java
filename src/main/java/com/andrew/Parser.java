package com.andrew;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Assumptions: 
 * 
 * 1. The supplied expected output shouldn't (can't) be 100% emulated since its currency 
 * formatting is inconsistent. (Sometimes the format is Rands and Cents, other times just Rands)
 *
 * Below is the output from the unit test written to drive my development
 *
 * --------------------------------------------------------------
 * line 3: actualLine not equal to expectedLine
 * line 3: actualLine:'3,Bag of ice,5,R10.00,R1.40,R57.00'
 * line 3: expectedLine:'3,Bag of ice,5,R10,R1.40,R57'
 *
 * line 4: actualLine not equal to expectedLine
 * line 4: actualLine:'4,2l Coke,2,R20.00,R2.80,R45.60'
 * line 4: expectedLine:'4,2l Coke,2,R20,R2.80,R45.60'
 * --------------------------------------------------------------
 * However, on line 2, the expected unit price is R5.00, not R5, so there seems to me to be no
 * consistent rule for this that I can implement.
 * 
 * 2. Although use of an external CSV-parsing library is a better idea for a production system,
 * I've deemed this just a distraction for the purposes of this exercise.
 * 
 * 3. For error handling, I've just coded up some simple messages to stdout. 
 *    Without more information about the production environment in which this would run it doesn't 
 *    feel like I can do much more.
 *    
 * @author andrew
 *
 */
public class Parser {
	
	private static final String _2F = "%.2f";
	private static final String COMMA_SEPARATOR = ",";

	public static void main(String[] args) throws IOException {
		
		try {
			
			String inputFilepath = "src/main/resources/invoiceItems.txt";
			Parser parser = new Parser();		
			List<String> actualLines = parser.buildOutputFromCsv(inputFilepath);
			for (String s: actualLines){
				System.out.println(s);
			}
			
		} catch (IOException e) {
			System.out.println("ERROR: Processing aborted. IOException:"+e);
		}

		
	}
	
	/**
	 * Builds the output as a List of String lines, given the inputFilename
	 * 
	 * @param inputFilename
	 * @return
	 * @throws IOException
	 */
	public List<String> buildOutputFromCsv(String inputFilename) throws IOException{
		
		//load inputFilename into a List of String lines
		Path path = FileSystems.getDefault().getPath(".", inputFilename);
		List<String> inputLines = Files.readAllLines(path, Charset.defaultCharset());

		//Set up data structures
		Map<String,Item> uniqueItemDetails = new TreeMap<String,Item>();
				
		//Populate these data structures while  iterating through the inputFilename
		buildItemCountsAndUniqueItemDetails(inputLines, uniqueItemDetails);
		
		//Set up running totals for use in the last line of the output
		BigDecimal totalVat = new BigDecimal("0");
		BigDecimal totalLineTotal = new BigDecimal("0");

		//Now build up the output file as a List of String lines
		List<String> out = new ArrayList<String>();
		out.add("Product Id,Description,Count,Unit Price,Vat,Line Total");
		
		//Making itemCounts a TreeMap ensures that the iterated keys will be ordered by productId
		for (String productId:uniqueItemDetails.keySet())
		{
			Item item = uniqueItemDetails.get(productId);
			Integer count = item.getCount();
			BigDecimal lineTotal = new BigDecimal(count).multiply(item.getUnitPricePlusVat());

			out.add(new StringBuilder(productId).append(COMMA_SEPARATOR)
					.append(item.getDescription()).append(COMMA_SEPARATOR)
					.append(count).append(",R")
					.append(String.format(_2F, item.getUnitPrice())).append(",R")
					.append(String.format(_2F, item.getVat())).append(",R")
					.append(String.format(_2F,lineTotal)).toString()
					);
			
			//increment totals for display in the last line
			totalVat = totalVat.add(item.getVat());
			totalLineTotal = totalLineTotal.add(lineTotal);
		}

		//and add on the last line
		out.add(
				new StringBuilder("Total,,,,R")
				.append(String.format(_2F,totalVat))
				.append(",R").append(String.format(_2F,totalLineTotal))
				.toString()
				);
		return out;
		
	}

	/**
	 * Iterate over inputLines and populate uniqueItemDetails which is keyed by productId
	 * 
	 * @param inputLines
	 * @param uniqueItemDetails
	 */
	private void buildItemCountsAndUniqueItemDetails(List<String> inputLines, Map<String, Item> uniqueItemDetails) {
		int lineNumber=0;
		for (String inputLine: inputLines)
		{
			lineNumber++;
			
			if(lineNumber==1){
				continue;
			}
			
			String[] vals = inputLine.split(COMMA_SEPARATOR);
			if (vals.length < 3){
				throw new RuntimeException("ERROR, lineNumber:"+lineNumber+", problem parsing line with contents:'"+inputLine+"'");
				
			}
			String productId = vals[0];
			String unitPrice = vals[1].trim();
			String description = vals[2].trim();
			
			if (uniqueItemDetails.containsKey(productId)){
				uniqueItemDetails.get(productId).incrementCount();
			} else {
				try {
					uniqueItemDetails.put(productId, new Item(productId, description, unitPrice));
				} catch (NumberFormatException e) {
					throw new RuntimeException("ERROR, lineNumber:"+lineNumber+", problem setting unitPrice to value of: '"+unitPrice+"'. Check the CSV file.");
				}
			}
	
		}
	}

}