require 'csv'
require 'bigdecimal'
require 'pry'

def format_as_money big_dec_value
	return "R#{'%.2f' % big_dec_value}"
end

input_filename = 'invoiceItems.txt'
expected_output_filename = 'expected.txt'

#Assumptions:
#
#
#
# TODO
# The program should be well designed, handle errors and should be of 
# sufficient quality to run on a production system. Please indicate all assumptions made.
# So write it as a command-line filter?

item_counts = Hash.new

unique_item_details = Hash.new

line_count = 0
CSV.foreach(input_filename) do |row| 

	line_count=line_count+1
	next if line_count==1

	product_id = row[0]

	if (item_counts.has_key?(product_id))
		current_count = item_counts[product_id]
		item_counts[product_id] = current_count + 1
	else
		item_counts[product_id] = 1
		unit_price = BigDecimal.new(row[1],2)
		vat = 0.14 * unit_price
		unique_item_details[product_id] = {'unit_price' => unit_price, 'description' => row[2].strip, 'vat' => vat}
	end

end

total_vat = 0
total_line_total = 0


actual = Array.new
actual << "Product Id,Description,Count,Unit Price,Vat,Line Total\n"

item_counts.keys.sort.each do |product_id|

	count = item_counts[product_id]
	item_details = unique_item_details[product_id] 
	line_total = count*(item_details['unit_price']+item_details['vat'])

	total_vat = total_vat + item_details['vat']
	total_line_total = total_line_total + line_total

	actual << "#{product_id},#{item_details['description']},#{count},#{format_as_money(item_details['unit_price'])},#{format_as_money(item_details['vat'])},#{format_as_money(line_total)}\n"

end

actual << "Total,,,,R#{'%.2f' % total_vat},R#{'%.2f' % total_line_total}"

expected = Array.new
File.open(expected_output_filename).each_line do |line|
    expected << line
end

#now compare actual to expected

if (actual.size != expected.size)
	puts "actual line count:#{actual.size} not the same as expected line count:#{expected.size}"
end


actual.each_with_index do |line, index|

	# binding.pry
	unless (line == expected[index])
		puts "line: #{index}, actual line NOT equal to expected line"
		puts "actual:'#{line}'\nexpected:'#{expected[index]}'\n\n"
	else
		# puts "line: #{index} is equal to expected line"		
	end

end

# puts "actual:\n#{actual}\n\n"
# puts "expected:\n#{expected}"

